-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: libpaws-perl
Binary: libpaws-perl
Architecture: all
Version: 0.42-1
Maintainer: Per Weijnitz <per.weijnitz@arbetsformedlingen.se>
Homepage: https://metacpan.org/release/Paws
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 11), perl
Package-List:
 libpaws-perl deb perl optional arch=all
Checksums-Sha1:
 d0c7a85c3fc619bc935792fdd93746624db8a3ca 8135332 libpaws-perl_0.42.orig.tar.gz
 69d69bcc00689b1a1b20f78fae3d81add8de93ef 1664 libpaws-perl_0.42-1.debian.tar.xz
Checksums-Sha256:
 ee52799ee559658c404cea8d27160e9ed418debe6a77ba455de99bd8c8ca5063 8135332 libpaws-perl_0.42.orig.tar.gz
 368ea7d0dc890086f82910c320a444bccad2424674c595d69c231454069e5a7c 1664 libpaws-perl_0.42-1.debian.tar.xz
Files:
 9703702e69933d001d75fc389ba31cea 8135332 libpaws-perl_0.42.orig.tar.gz
 a033b66a1d67a5f5445808806ac6f429 1664 libpaws-perl_0.42-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEE6niSkXgqx29tzEorbrTKOWRu0agFAmCIRd0ACgkQbrTKOWRu
0ajb9Qv+IgjLrJIFF4n3cGc2XcEweG1zvMRInrGwCZumxSegIs1n70e0BLyEeHwt
u2kpAM1VkNd/ce/vw2yJyjXBXlc9Zb1IkbuvjxHjUwPO00pO+or5bYXZ65W8hmow
RLCiK16hcboT9Aw6fV92hDPtCpfr1/QTO2wucAviR70bx4jtLhXV+IF4ta3cR38N
QgQJOq9JRzDHCT1B9tdUAXIds/dqFXJk1RQFhDoeUsjANy9iPd4gRCYC2D9M8jax
6S8hYywNu8daJNVGMThbACpNwrTmeODWRDygjXGHQrW/ES6LErwqy4NIeIan8yIP
2z+f9fMvHcx9zQOgGKo6Q04P/JauP9Bb/MGb6W0ZEOfr0iJlgmAS5KCSPVmtoUNe
N9lHbYvag8kpZlvJqx2XYbQZ7UeXvdAJjRLHv70jzx5fubBxOr4iECno3Y9gwFAq
VbtX1EM0DuFfagCkVVGwvd59oGpWMQbWQsoBMZNJVMnXwv+JXs0rjbye/NAK87Az
uGg/Bqpu
=rFY9
-----END PGP SIGNATURE-----
