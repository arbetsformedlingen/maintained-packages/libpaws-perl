#!/usr/bin/env bash
set -eEu -o pipefail

cd /tmp

curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/master/pkgs/libfuture-mojo-perl_1.001-1_all.deb
curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/master/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb

apt install -f ./libnet-amazon-signature-v4-perl_0.21-1_all.deb ./libfuture-mojo-perl_1.001-1_all.deb
